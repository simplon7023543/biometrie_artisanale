
import plotly.express as px
from file_import import dt_all_data

tranche_age=[{'min':15,'max':20},
             {'min':20,'max':25},
             {'min':25,'max':30},
             {'min':30,'max':35},
             {'min':35,'max':40},
             {'min':40,'max':45},
             {'min':45,'max':50},
             {'min':50,'max':55},
             {'min':55,'max':60},
             {'min':60,'max':65},
             {'min':65,'max':70}]

# exe2 :Un camembert de répartition de l’échantillon par tranche d’age de 5 ans.
repartition_age = []
for i in tranche_age:
    valeur = dt_all_data.loc[(dt_all_data['age'] >= i['min']) & ((dt_all_data['age'] < i['max']))]
    # print(str(i) + " = " + str(valeur['age'].count()))
    repartition_age.append({'tranche':str(i['min'])+"-"+str(i['max']),"valeur":valeur['age'].count()})
# print(repartition_age)
fig = px.pie( repartition_age,values="valeur",names="tranche",title=" ex2 : Un camembert de répartition repartition age")
fig.show()


# exe3 :Le même camembert avec pour chaque tranche une distinction femme / homme
lst = dt_all_data.loc[(dt_all_data['age'] >15) & ((dt_all_data['age'] < 20))]
repartition_age_gender = []
for i in tranche_age:
    valeur = dt_all_data.loc[( dt_all_data['age'] >= i['min']) & ((dt_all_data['age'] < i['max']))]
    count_by_gender = valeur.groupby(['gender']).count()
    # data_graph = {'tranche': str(i['min']) + "-" + str(i['max']), 'homme':count_by_gender['age'][0], 'femme':count_by_gender['age'][1]}
    data_graph = {'tranche': str(i['min']) + "-" + str(i['max']) + ' - homme', 'qte': count_by_gender['age'][0]}
    repartition_age_gender.append(data_graph)
    data_graph = {'tranche': str(i['min']) + "-" + str(i['max']) + ' - femme', 'qte': count_by_gender['age'][1]}
    repartition_age_gender.append(data_graph)

# print(repartition_age_gender)
fig = px.pie( repartition_age_gender, names='tranche', values='qte', title="exe3 :Le même camembert avec pour chaque tranche une distinction femme / homme")
fig.show()

