import pandas
import plotly.express as px
from file_import import dt_all_data

# 10 : Un histogramme qui compare la taille du biceps en fonction de la taille ;
# 'bic_gir'
# 'height'

dt_taille_biceps = dt_all_data[['height','bic_gir']].groupby('height').mean()
fig = px.bar(dt_taille_biceps,labels={'height':'taille','value':'taille biceps','variable':'moyenne taille byceps'} ,title='ex10 : Un histogramme qui compare la taille du biceps en fonction de la taille')
fig.show()

# 11 : Un histogramme qui compare la taille du biceps en fonction de la age ;
# 'bic_gir'
# 'age'
dt_taille_biceps_age = dt_all_data[['age','bic_gir']].groupby('age').mean()
fig = px.bar(dt_taille_biceps_age,labels={'age':'age','value':'taille biceps','variable':'moyenne taille byceps'} ,title='ex11 : Un histogramme qui compare la taille du biceps en fonction de l''age')
fig.show()
