import plotly.express as px
from file_import import dt_all_data
# Pour chaque personne,
# calculer la moyenne des différences entre
# 58 - 62 et
# 64 – 68
# et entre
# 64 – 68 et
# 70 – 74.
# Puis calculer la moyenne de ces moyennes pour les femmes et pour les hommes.
# Puis afficher ces deux valeurs dans un histogramme ;
# 'wai_gir',
# 'nav_gir',
# 'hip_gir',

# 12 --> 'wai_gir'
# 58 - 62   Waist girth, narrowest part of torso below the rib cage,
#             average of contracted and relaxed position
# 13 --> 'nav_gir'
# 64 - 68   Navel (or "Abdominal") girth at umbilicus and iliac crest,
#             iliac crest as a landmark
# 14 -->'hip_gir'
# 70 - 74   Hip girth at level of bitrochanteric diameter
def femme_homme(valeur):
    if valeur['gender']=='0':
        valeur['gender'] = "femme"
    else:
        valeur['gender'] = "homme"
    return valeur

dt_filtre_14 = dt_all_data[['gender','wai_gir','nav_gir', 'hip_gir']]

sub_1= dt_filtre_14['wai_gir'].subtract(dt_filtre_14['nav_gir'])

sub_2= dt_filtre_14['nav_gir'].subtract(dt_filtre_14['hip_gir'])


dt_filtre_14.insert(4,'diff_58_64',sub_1)
dt_filtre_14.insert(5,'diff_64_70',sub_2)
dt_filtre_14.insert(6,'moyenne_de_diff',(dt_filtre_14['diff_58_64']+dt_filtre_14['diff_64_70'])/2)

df_moyenne_moyenne_hf = dt_filtre_14[['gender','moyenne_de_diff']]
df_moyenne_moyenne_hf = df_moyenne_moyenne_hf.astype({'gender':str,'moyenne_de_diff':float})
df_moyenne_moyenne_hf = df_moyenne_moyenne_hf.apply(lambda valeur: femme_homme(valeur), axis=1)
df_moyenne_moyenne_hf = df_moyenne_moyenne_hf.groupby('gender').mean()
print(df_moyenne_moyenne_hf)

fig = px.bar(df_moyenne_moyenne_hf, title="exercice 14")
fig.show()
