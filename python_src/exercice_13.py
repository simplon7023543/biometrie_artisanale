import plotly.express as px
from file_import import dt_all_data

# 13 : Un nuage de points des poids en fonction de l’age.
# Avec des couleurs différentes pour les femmes et les hommes ;
# 'weight'
# 'gender'
def femme_homme(valeur):
    if valeur['gender']=='0':
        valeur['gender'] = "femme"
    else:
        valeur['gender'] = "homme"
    return valeur

dt_points_age = dt_all_data[['gender','age', 'weight']]
dt_points_age = dt_points_age.astype({'gender':str,'age':float, 'weight':float})


dt_points_age = dt_points_age.apply(lambda valeur: femme_homme(valeur), axis=1)
print(dt_points_age)
fig = px.scatter(dt_points_age,x='age',y='weight',color='gender',title="Un nuage de points des poids en fonction de l’age. Avec des couleurs différentes pour les femmes et les hommes ;")
fig.show()
