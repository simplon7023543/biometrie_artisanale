import pandas
import plotly.express as px
from file_import import dt_all_data

#7: Courbe de croissance moyenne (taille moyenne en fonction de l’age) ;

dt_age = dt_all_data.groupby(['age'])['height']
courbe_age_taille=[]
for i in dt_age:
    courbe_age_taille.append( {"age":int(i[0][0]),"taille_moyene":i[1].mean()})

fig = px.line(courbe_age_taille,x="age",y="taille_moyene", title="exe7: Courbe de croissance moyenne (taille moyenne en fonction de l’age)")
fig.show()

#8: Courbe de croissance moyenne (taille moyenne en fonction de l’age) ;
dt_age = dt_all_data.groupby(['age'])['weight']
courbe_age_poids=[]
for i in dt_age:
    courbe_age_poids.append( {"age":int(i[0][0]),"poids_moyen":i[1].mean()})

fig = px.line(courbe_age_poids,x="age",y="poids_moyen", title="exe8: Courbe de poids moyen (poids moyen en fonction de l’age)")
fig.show()