# Est-ce que les personnes qui ont un IMC > 20 ont de plus grosses chevilles
# que les autres Créer un diagramme qui démontre votre réponse
# 'dia_ank'
import pandas
import plotly.express as px
from file_import import dt_all_data
def calcul_imc(taille:float,poids:float)-> float:
    return (poids/(taille*taille))*10000

dt_data_imc_ank = pandas.DataFrame(dt_all_data[['dia_ank','height', 'weight']],index=None)

dt_data_imc_ank.insert(3,"imc",calcul_imc(dt_data_imc_ank['height'],dt_data_imc_ank['weight']))
dt_data_imc_ank = dt_data_imc_ank.astype({'dia_ank':float,'height':float,"weight":float,"imc":float})
print(dt_data_imc_ank)

dt_imc_sup_20 = dt_data_imc_ank.loc[dt_data_imc_ank['imc'] > 20.0]
dt_imc_sup_20.insert(4,'imc 20','imc > 20')
dt_imc_inf_20 = dt_data_imc_ank.loc[dt_data_imc_ank['imc'] <= 20.0]
dt_imc_inf_20.insert(4,'imc 20','imc < 20')

pd_for_graph = pandas.concat([dt_imc_sup_20[['dia_ank',"imc", 'imc 20']], dt_imc_inf_20[['dia_ank',"imc", 'imc 20']]])
# for j in pd_for_graph.values:
#     print(str(i[0]) + "\t" + str(i[1]) + "\t"+ str(i[2]) )

fig = px.scatter(pd_for_graph,x='imc',y='dia_ank', color='imc 20', title='exercice 19 : Est-ce que les personnes qui ont un IMC > 20 ont de plus grosses chevilles')
fig.show()