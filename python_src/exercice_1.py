
# colonne:pos: description
# "dia_Bia";(1,4);Biacromial diameter
# "dia_Bii";(6,9);Biiliac diameter
# "dia_Bit";(11,14);Bitrochanteric diameter
# "ches_dep_spi_ster";(16,19);Chest depth between spine and sternum at nipple level
# "ches_dia_nip_exp";(21,24);Chest diameter at nipple level, mid expiration
# "dia_elb";(26,29);Elbow diameter, sum of two elbows
# "dia_wri";(31,34);Wrist diameter, sum of two wrists
# "dia_knee";(36,39);Knee diameter, sum of two knees
# "dia_ank";(41,44);Ankle diameter, sum of two ankles
# "sho_delt_mus";(46,50);Shoulder girth over deltoid muscles
# "ches_gir";(52,56);Chest girth, nipple line in males and just above breast tissue in females, midexpiration
# "wai_gir";(58,62);Waist girth, narrowest part of torso below the rib cage, average of contracted and relaxed position
# "nav_gir";(64,68);Navel girth at umbilicus and iliac crest,iliac crest as a landmark
# "hip_gir";(70,74);Hip girth at level of bitrochanteric diameter
# "thi_gir";(76,79);Thigh girth below gluteal fold, average of right and left girths
# "bic_gir";(81,84);Bicep girth, flexed, average of right and left girths
# "for_gir";(86,89);Forearm girth, extended, palm up, average of right and left girths
# "kne_gir";(91,94);Knee girth over patella, slightly flexed position, average of right and left girths
# "cal_max_gir";(96,99);Calf maximum girth, average of right and left girths
# "ank_min_gir";(101,104);Ankle minimum girth, average of right and left girths
# "wri_min_gir";(106,109);Wrist minimum girth, average of right and left girths
# "age";(111,114);Age (years)
# "weight";(116,120);Weight (kg)
# "height";(122,126);Height (cm)
# "gender";(128,128);Gender (1 male, 0 female)

import pandas
import plotly.express as px
from file_import import dt_all_data
dt_all_data.index = pandas.Index(dt_all_data['gender'],name='hf')
val = dt_all_data['gender'].value_counts()
l_val= val.tolist()

proportion_homme_femme = [{"gender":"homme","valeur":l_val[0]}, {"gender":"femme","valeur":l_val[1]}]

print(proportion_homme_femme)
dt_hf = pandas.DataFrame(proportion_homme_femme)
print(dt_hf)

fig = px.pie( dt_hf,values="valeur",names="gender",title=" ex1 : Un camembert de répartition femme / homme dans l’échantillon")
fig.show()