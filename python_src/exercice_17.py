import pandas
import plotly.express as px
from file_import import dt_all_data
# exercice 17 :  Calculer l’IMC de chaque personne et afficher dans un tableau :
# Taille, Poids, IMC, pour les personnes dont l’IMC > 20 ;
# https://www.business-plan-excel.fr/imc-excel-calculateur-formule-graphique/
# IMC = poids (kg) / (taille (cm) x taille (cm))
def calcul_imc(taille:float,poids:float)-> float:
    return (poids/(taille*taille))*1000

dt_data_imc = pandas.DataFrame(dt_all_data[['height', 'weight']],index=None)
dt_data_imc = dt_data_imc.astype({'height':float,"weight":float})
dt_data_imc.insert(2,"imc",calcul_imc(dt_data_imc['weight'],dt_data_imc['height']))
dt_data_imc = dt_data_imc.astype({'height':float,"weight":float,"imc":float})


dt_data_imc = dt_data_imc.sort_values(by=['height','weight'], ascending=True)

print("exercice 17 : personnes avec imc > 20")
print(dt_data_imc.loc[dt_data_imc["imc"] >20])
# print("height\tweight\timc")
# for i in dt_data_imc.loc[dt_data_imc['imc'] >20.0].values:
#     print(str(i[0]) + "\t" + str(i[1]) + "\t" + str(i[2]))



