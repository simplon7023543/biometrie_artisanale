import plotly.express as px
from file_import import dt_all_data

def femme_homme(valeur):
    if valeur['gender']=='0':
        valeur['gender'] = "femme"
    else:
        valeur['gender'] = "homme"
    return valeur

df_homme_femmes_bi_moyenne = dt_all_data[['gender','bic_gir']]
print(df_homme_femmes_bi_moyenne)
df_homme_femmes_bi_moyenne = df_homme_femmes_bi_moyenne.astype({'gender':str,'bic_gir':float})
df_homme_femmes_bi_moyenne = df_homme_femmes_bi_moyenne.apply(lambda valeur: femme_homme(valeur), axis=1)
df_homme_femmes_bi_moyenne = df_homme_femmes_bi_moyenne.groupby('gender').mean()

print(df_homme_femmes_bi_moyenne)

