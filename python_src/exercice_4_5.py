import plotly.express as px
from file_import import dt_all_data
import pandas

df_femmes_hommes_tailles =  dt_all_data[["height"]].loc[dt_all_data["gender"] < 1]
df_femmes_hommes_tailles.index = pandas.Index()
df_femmes = df_femmes_hommes_tailles.sort_values(by="height",  ascending=True)
# print(df_femmes_hommes_tailles)
fig = px.bar(df_femmes, y="height")
fig.update_xaxes(type='category')
fig.show()